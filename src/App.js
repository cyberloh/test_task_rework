import './App.css';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import { ApiProvider } from "./contexts/ApiContext";
import TaskList from './components/TaskList';
import Login from './components/Login';
import Navbar from './components/Navbar';
import TaskEditor from './components/TaskEditor';
import TaskCreator from './components/TaskCreator';

function App() {

  return (
    <div className="App">
      <Router>
        <ApiProvider>
            <Navbar />
            <Switch>
              <Route path='/' exact component={TaskList}/>
              <Route path='/login' exact component={Login}/>
              <Route path='/edit' exact component={TaskEditor}/>
              <Route path='/create' exact component={TaskCreator}/>
            </Switch>
        </ApiProvider>
      </Router>
    </div>
  );
}

export default App;
