import React, { useContext, useEffect, useState } from 'react';
import { ApiContext } from "../contexts/ApiContext";
import { Form, Input, Button, Space, Select, Typography } from 'antd';

const { Text } = Typography;
const { TextArea } = Input;
const { Option } = Select;

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 8 },
};

const tailLayout = {
  wrapperCol: { offset: 8, span: 8 },
};

export default function TaskForm({task, onFinish }) {

  const { appState, dispatch } = useContext(ApiContext);
  const [edited, setEdited] = useState(task.status === 1 || task.status === 11);
  const [taskStatus, setTaskStatus] = useState((task?.status || 0).toString());
  const [form] = Form.useForm();

  useEffect(() => {
    dispatch( { type: 'setTasksError', payload: '' } )
    dispatch( { type: 'setTasksResult', payload: '' } )
  }, []);

  const onStatusChange = statusValue => {
    setTaskStatus(statusValue);
    form.setFieldsValue({ status: statusValue });
    if(statusValue === '0' || statusValue === '10')
      setEdited(false);
  };

  const onTaskTextChange = () => {
    setEdited(true);
    if(taskStatus === '0' || taskStatus === '10'){
      const newStatus = (parseInt(taskStatus) + 1).toString();
      setTaskStatus(newStatus);
      form.setFieldsValue({ status: newStatus });
    }
  };

  return (
    <>
      <Form
        {...layout}
        name="basic"
        initialValues={{...task, status: taskStatus}}
        form={form}
        onFinish={onFinish}
      >
        <Form.Item
          label="Имя пользователя"
          name="username"
          rules={[{ required: true, message: 'Пожалуйста, укажите имя пользователя' }]}
        >
          <Input disabled={task.id}/>
        </Form.Item>

        <Form.Item
          label="Email"
          name="email"
          rules={[{ required: true, type: 'email', message: 'Пожалуйста, укажите email' }]}
        >
          <Input disabled={task.id}/>
        </Form.Item>


        <Form.Item
          label="Текст"
          name="text"
          rules={[{ required: true, message: 'Пожалуйста, укажите текст задания' }]}
        >
          <TextArea onChange={onTaskTextChange}/>
        </Form.Item>

        { task.id &&
          <Form.Item
            label="Статус"
            name="status"
          >
            <Select
              onChange={onStatusChange}
              defaultValue={ task.status.toString() }
              value={ taskStatus }
            >
            { !edited &&
              <>
                <Option value='0'>Не выполнена</Option>
                <Option value='10'>Выполнена</Option>
              </>
            }
            { edited &&
              <>
                <Option value='1'>Не выполнена, отредактирована администратором</Option>
                <Option value='11'>Выполнена, отредактирована администратором</Option>
              </>
            } 
            </Select>
          </Form.Item>
        }

        <Form.Item {...tailLayout}>
          <Space>
          <Button type="primary" htmlType="submit">
            Сохранить
          </Button>
          <Button type="default" htmlType="cancel" href="/">
            Закрыть
          </Button>
          </Space>
        </Form.Item>
      </Form>
      { appState.tasksError.length > 0 && <Text type="danger">{appState.tasksError}</Text> }
      { appState.tasksResult.length > 0 && <Text type="success">{appState.tasksResult}</Text> }
    </>
  )
};
