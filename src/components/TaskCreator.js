import React, { useContext } from 'react';
import { ApiContext } from "../contexts/ApiContext";
import TaskForm from './TaskForm';

export default function TaskCreator() {

  const  { taskCreate } = useContext(ApiContext);

  const onFinish = (formData) => {
    taskCreate(formData);
  };

  return (
    <TaskForm task={{}} onFinish={onFinish}/>
  )
};
