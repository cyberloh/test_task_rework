import React, { useContext } from 'react';
import { ApiContext } from "../contexts/ApiContext";
import { Button, Table, Typography, Select, Space } from "antd";

const { Text } = Typography;

const { Option } = Select;

const statusDescriptions = {
  0: 'Не выполнена',
  1: 'Не выполнена, отредактирована администратором',
  10: 'Выполнена',
  11: 'Выполнена, отредактирована администратором'
};

export default function TaskList({history}){

  const {appState, dispatch } = useContext(ApiContext);

  const onTaskEditClick = task => {
    dispatch({ type: 'setActiveTask', payload: task });
    history.push('/edit');
  };

  const onSortFieldChange = sortField => {
    dispatch({type: 'setTasksSortField', payload: sortField });
  };

  const onSortDirectionChange = sortDirection => {
    dispatch({type: 'setTasksSortDirection', payload: sortDirection });
  };

  const onPaginationChange = pageNum => {
    dispatch({ type: 'setTasksPageNum', payload: pageNum });
  };
  
  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      key: 'id'
    },
    {
      title: 'Пользователь',
      dataIndex: 'username',
      key: 'username'
    },
    {

      title: 'Email',
      dataIndex: 'email',
      key: 'email'
    },
    {

      title: 'Текст',
      dataIndex: 'text',
      key: 'text'
    },
    {

      title: 'Статус',
      dataIndex: 'status',
      key: 'status',
      render: status => statusDescriptions[status] || ''
    },
    {
      title: 'Действие',
      dataIndex: 'id',
      key: 'id',
      render: (text, task) => <Button onClick={() => onTaskEditClick(task)}>Изменить</Button>
    }
  ];

  const pagination = {
    defaultCurrent: 1,
    current: appState?.tasksPageNum || 1,
    pageSize: 3,
    total: appState.tasks?.total_task_count || 0,
    onChange: onPaginationChange
  };

  return(
    <>
      <div className='TasksToolbar'>
        <Button href='/create'>Добавить задание</Button>
        <Space>
          <Text>Сортировка:</Text>
          <Select
            dropdownMatchSelectWidth={false}
            value={appState.tasksSortField}
            placeholder='Сортировка по полю'
            onChange={onSortFieldChange}
          >
            <Option value="none">нет</Option>
            <Option value="id">id</Option>
            <Option value="username">username</Option>
            <Option value="email">email</Option>
            <Option value="status">status</Option>
          </Select>
          { appState.tasksSortField !== 'none' &&
            <Select
              value={appState.tasksSortDirection}
              placeholder='Направление'
              onChange={onSortDirectionChange}
            >
              <Option value="asc">от меньшего</Option>
              <Option value="desc">от большего</Option>
            </Select>
          }
        </Space>
      </div>
      <Table 
        columns={columns} 
        dataSource={appState.tasks.tasks} 
        pagination={pagination}
        rowKey='id'
      />
    </>
  );
}
