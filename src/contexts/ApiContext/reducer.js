import localStorageService from '../../services/localstorage.service';

export const initialState = {
  loginError: '',
  tasksError: '',
  tasksResult: '',
  tasks: {},
  tasksPageNum: 1,
  tasksSortField: 'none',
  tasksSortDirection: 'desc',
  authKey: localStorageService.authKey(),
  activeTask: {},
};

export const reducer = (state = initialState, action) => {


  if(action.type === 'setAuthKey'){
    localStorageService.setAuthKey(action.payload);
    return { ...state, authKey: action.payload }
  };

  if(action.type === 'logout'){
    localStorageService.deleteAuthKey();
    return { ...state, authKey: null }
  };

  if(action?.type.startsWith('set')){
    let paramName = action.type.slice(3);
    paramName = paramName.charAt(0).toLowerCase() + paramName.slice(1);
    return { ...state, [paramName]: action.payload };
  }


  return state;
    
};
