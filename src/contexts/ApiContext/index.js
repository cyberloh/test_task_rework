import React, { createContext, useEffect, useReducer } from 'react';
import { useHistory } from 'react-router-dom';
import { reducer, initialState } from "./reducer";
import taskService from '../../services/task.service';
import localStorageService from '../../services/localstorage.service';
import authService from '../../services/auth.service';

export const ApiContext = createContext();

export const ApiProvider = ({children}) => {

  const history = useHistory();

  const [state, dispatch] = useReducer(reducer, initialState);

  const fetchTasks = () => {
    taskService.tasks(state.tasksPageNum, state.tasksSortField, state.tasksSortDirection)
    .then(tasks => dispatch({type: 'setTasks', payload: tasks}));
  };

  const taskCreate = taskData => {
    taskService.create(taskData)
      .then(() => {
        fetchTasks();
        dispatch({ type: 'setTasksResult', payload: 'Задание добавлено' });
      })
      .catch(err => {
        if(err instanceof Object)
          err = JSON.stringify(err);
        dispatch({ type: 'setTasksError', payload: err });
      });
  };

  const taskUpdate = (taskUpdates) => {
    taskService.update(state.activeTask.id, {
      text: taskUpdates.text, 
      status: taskUpdates.status,
      token: localStorageService.authKey() })
      .then(() => {
        fetchTasks();
        dispatch({ type: 'setTasksResult', payload: 'Задание сохранено' });
      })
      .catch(err => {
        if(err instanceof Object)
          err = JSON.stringify(err);
        dispatch({ type: 'setTasksError', payload: err });
      });
  };

  const login = credentials => {
    authService.login(credentials)
      .then(res => {
        dispatch({type: 'setAuthKey', payload: res?.token});
        history.push('/');
      })
      .catch(err => {
        if(err instanceof Object)
          err = JSON.stringify(err);
        dispatch({ type: 'setLoginError', payload: err });
      });
  };

  const logout = () => {
    dispatch({ type: 'logout' })
  };

  useEffect(() => fetchTasks(), []);
  useEffect(() => fetchTasks(), [
    state.tasksSortField, 
    state.tasksSortDirection,
    state.tasksPageNum
  ]);

  return(
    <ApiContext.Provider
      value={{
        appState: state,
        dispatch,
        fetchTasks,
        taskCreate,
        taskUpdate,
        login,
        logout
      }}
    >
      {children}
    </ApiContext.Provider>
  )
};
