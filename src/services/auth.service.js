import httpService from './http.service';

const authService = {

  login(credentials){
    return httpService.post('login/', credentials);
  }

};

export default authService;
