let baseApiUri = '';

if (process.env.NODE_ENV === 'development')
  baseApiUri = process.env.REACT_APP_DEV_API_URI;

if (process.env.NODE_ENV === 'production')
  baseApiUri = process.env.REACT_APP_PROD_API_URI;

const developer = process.env.REACT_APP_DEVELOPER;

const formSubmit = (endpointUri, data, method) => {

  const headers = {
    Accept: 'application/json'
  };

  const formData = new FormData();
  Object.keys(data).forEach(key => formData.append(key, data[key]));
  
  return fetch(baseApiUri + endpointUri, {
      mode: 'cors',
      method,
      headers,
      body: formData
    }
  )
  .then(res => res.json())
  .then(json => {
    if(json?.status !== 'ok')
      throw json?.message || 'unknown error';
    return json?.message;
  });
};

const endpointUriNormalize = endpointUri => {
  if(endpointUri.indexOf('developer=') === -1){
    if(endpointUri.indexOf('?') === -1)
      endpointUri += '?';
    else
      endpointUri += '&';
    endpointUri +=  `developer=${developer}`;
  }
  return endpointUri;
}

const httpService = {

  post(endpointUri, data) {
    endpointUri = endpointUriNormalize(endpointUri);
    return formSubmit(endpointUri, data, 'POST');
  },

  patch(endpointUri, data) {
    endpointUri = endpointUriNormalize(endpointUri);
    return formSubmit(endpointUri, data, 'PATCH');
  },
  
  get(endpointUri){
    endpointUri = endpointUriNormalize(endpointUri);
    return fetch(baseApiUri + endpointUri, {
        method: 'GET',
        mode: 'cors',
        headers: {
          Accept: 'application/json',
        },
      }
    )
    .then(res => res.json())
    .then(json => {
      if(json?.status !== 'ok')
        throw json?.message || 'unknown error';
      return json?.message;
    });
    
  },
};

export default httpService;
