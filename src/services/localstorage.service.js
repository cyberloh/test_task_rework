const localStorageService = {

  setAuthKey(authKey){
    localStorage.setItem('authKey', authKey);
  },

  authKey(){
    return localStorage.getItem('authKey')
  },

  deleteAuthKey(){
    localStorage.removeItem('authKey')
  }

};

export default localStorageService;
