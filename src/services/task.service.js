import httpService from './http.service';

const buildSortQuery = ( sortField, sortDirection ) => {
  if(sortField === 'none')
    return '';
  return `&sort_field=${sortField}&sort_direction=${sortDirection}`;
};

const taskService = {

  tasks(pageNum = 1, sortField = 'none', sortDirecton = 'desc'){
    let fetchUri =`?page=${pageNum}`;
    fetchUri += buildSortQuery(sortField, sortDirecton);
    return httpService.get(fetchUri);
  },

  task(id){
    return httpService.get(id + '/');
  },

  update(id, updates){
    return httpService.post(`edit/${id}/`, updates);
  },

  create(taskData){
    return httpService.post('create/', taskData);
  },

};

export default taskService;
